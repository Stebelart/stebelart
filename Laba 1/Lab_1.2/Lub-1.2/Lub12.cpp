// Lub12.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <cmath>

class Vector {
	int A, B;
	double length;
public:
	void SetA(int A) {
		this->A = A;
	}
	void SetB(int B) {
		this->B = B;
	}
	int GetA() {
		return A;
	}
	int GetB() {
		return B;
	}
	double Length() {
		length = sqrt(pow(A, 2) + pow(B, 2));
		return length;
	}
};

int main()
{
	Vector v;
	Vector subv;
	subv.SetA(3);
	v.SetA(2);
	v.SetB(2);
	std::cout<<v.GetA();
	std::cout << v.Length();
    return 0;
	system("pause");
}

