// Lub13.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>

using namespace std;

class BoxStr {
	string word;
	int pov = 0;
public:
	void SetValue(string word) {
		this->word = word;
	}
	string GetValue() {
		return word;
	}
	int SearchChar(char symbol) {
		for (int i = 0; i < word.length(); i++) {
			if (symbol == word[i])
			pov++;
		}
		return pov;
	}
};

int main()
{
	BoxStr a;
	a.SetValue("slovechko");
	cout<<a.GetValue() << endl;
	cout<< a.SearchChar('s')<<endl;

	system("pause");
    return 0;
}

