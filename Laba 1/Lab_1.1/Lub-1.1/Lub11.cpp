// Lub1.1.cpp : History String
//

#include "stdafx.h"
#include <iostream>
#include <string>

using namespace std;

class History {
	int size = 2;
	int itr = 0;
	string *arr = new string[size];
public:
	void Print(string text) {
		arr[itr] = text;
		cout << text<<endl;
		itr++;
	}
	void ShowHistory() {
		for (int i = 0; i < size; i++) {
			cout << arr[i] << "  ";
		}
	}

};
int main()
{
	History s;
	s.Print("qwerty");
	s.Print("543210");
	s.ShowHistory();

	system("pause");
    return 0;
}

