// Lab22.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream> 
template <class T>

T getMax(T arr[], int size) {
	T max = arr[0];
	for (int i = 0; i < size; i++) {
		if (arr[i] > max) {
			max = arr[i];
		}
	}
	return max;
}


int main()
{
	int arr[] = { 1, 2, 3, 4 ,5 ,6 ,7 ,8 };
	std::cout<< getMax<int>(arr, 8);

	system("pause");
	return 0;
}