// Lab21.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

class Vehicle {
public:
	void setNumber(int number) {
		this->number = number;
	}
	int getNumber() {
		return number;
	}
	void setModel(int model) {
		this->model = model;
	}
	int getModel() {
		return model;
	}
protected:
	int number;
	int model;
};

class Bike : public Vehicle {};
class Car : public Vehicle {};
class Bus : public Vehicle {};

int main(void) {
	Bike Kakoyto;
	Kakoyto.setNumber(1);
	
	return 0;
}

