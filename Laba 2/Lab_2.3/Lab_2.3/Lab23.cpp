// Lab23.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

template <class T>

class Calculator{
public:
	T Addition(T a, T b) {
		return a + b;
	}
	T Subtraction(T a, T b) {
		return a - b;
	}
	T Multiplication(T a, T b) {
		return a * b;
	}
    T Division(T a, T b) {
		return a / b;
	}
};



int main()
{
	Calculator <int> ForInt;
	Calculator <double> ForDouble;

	std::cout<< ForInt.Addition(2, 2)<<std::endl;
	std::cout << ForDouble.Division(5, 4) << std::endl;

	system("pause");
    return 0;
}

